package com.company;

import org.junit.Assert;
import org.junit.Before;
import org.junit.runner.notification.RunListener;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.junit.Test;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class LuckyNumberTest {

    LuckyNumber classObj;
    @Before
    public void setup() {
        classObj=new LuckyNumber();
    }

    @Test
    public void isValid(){
        int arr[]={2,62,162,8641,16293,175390,1098765,18907654,167890543,1432587960,-23};
        for(int element:arr){
            assertTrue(classObj.isLucky(element));
        }
    }
    @Test
    public void isInvalid(){
        int arr[]={2030,2131,2245,3734,3145454,999999999};
        for(int element:arr){
            assertFalse(classObj.isLucky(element));
        }
    }

    @Test(expected = NumberFormatException.class)
    public void wrongFormat(){
        String arr[]={"23s","gfgfss","534ACD"};

        for(String s:arr)
            assertFalse(classObj.isLucky(Integer.parseInt(s)));
    }


    @Mock
    FileRepository mockRepository;

    @Test
    public void mockTest(){

        MockitoAnnotations.initMocks(this);


        //STUB
        when(mockRepository.getFromFile(anyString())).thenReturn(getMockFile());
        
        LuckyNumber.isLucky(652);

        // Verify
        Assert.assertEquals(Integer.valueOf(12), mockRepository.getFromFile("input.txt").get(0));
        Mockito.verify(mockRepository).getFromFile("input.txt");
    }

    public List<Integer> getMockFile(){
        List<Integer> mockList = new ArrayList<>();
        mockList.add(12);  //stub creation

        return mockList;
    }
}
